angular.module('starter.controllers', [])

.controller('DashCtrl', function($scope, $rootScope, $state) {

  $scope.devices = [];

  $scope.startScanButton = function(){
    $rootScope.addLog("Start search");
    $scope.stopScanButton();


    evothings.ble.startScan(
      function(device)
      {
        $scope.applyDevice(device, null);
      },
      function(errorCode)
      {
        console.log(errorCode);
        // Report error.
        $scope.error(errorCode);
      }
    );
  }

  $scope.stopScanButton = function(){
    $rootScope.addLog("Stop search");
    evothings.ble.stopScan();
  }

  $scope.applyDevice = function(device) {
    var isAdded = true;
    for(var i=0; i < $scope.devices.length; i++) {
      if($scope.devices[i].address == device.address) {
        isAdded = false;
      }
    }
    if(isAdded) {
      $rootScope.addLog("Added device ("+device.name + ", "+device.address+")");
      $rootScope.addLog(JSON.stringify(device));
      $scope.$apply(function () {
            $scope.devices.push(device);
      });

    }

    $scope.connectToDevice = function(device) {
      $scope.stopScanButton();
      $rootScope.addLog('connect('+device.address+')');




  evothings.ble.connect(device.address, function(r)
  {
    alert("Connect sucess");
    $rootScope.addLog("Connect sucess");
    $rootScope.addLog(JSON.stringify(r));

    $rootScope.addLog('connect '+r.deviceHandle+' state '+r.state);
      if (r.state == 2) // connected
      {
        $rootScope.addLog('connected, requesting services...');
        $state.go('tab.chats', {device: device, connectResult:r});
        //app.getServices(r.deviceHandle);
      } else {
        $rootScope.addLog('State isnt 2 connected');
      }
    }, function(errorCode)
    {
      alert('connect error: ' + errorCode);
      $rootScope.addLog('connect error: ' + errorCode);
    });

      $rootScope.addLog('sent connect request to ('+device.address+')');
    }

  }

  $scope.error = function(error) {
    $rootScope.addLog("Error Search code:"+error);
    $scope.devices.push(error);
  }
})

.controller('ChatsCtrl', function($scope, $stateParams, $rootScope) {
  $rootScope.addLog(JSON.stringify($stateParams));

  // $scope.listServices = [{
  //   handle: "handle",
  //   type: 1,
  //   uuid: "uuid",
  //   characteristics : [
  //     {
  //       handle: "characteristics_handle",
  //       descriptors: [
  //         {
  //           handle: "desc_handle",
  //           uuid: "desc_uuid",
  //         }
  //       ]
  //     }
  //   ]
  //}];
  $scope.data = {
    ssid:"",
    password:""
  }


  $scope.listServices = [];

  $scope.setWifiConfig = function() {
    $rootScope.addLog("Set up wifi");
    $rootScope.addLog("Search needed services");
    var service = $scope.getServiceHandler();
    $rootScope.addLog("Finished needed services. Searched "+service);
    if(service !== undefined) {
      $scope.writeSSID($scope.data.ssid);
    }
  }


  $scope.enableNotification = function() {
    var service = $scope.getServiceHandler();
    var handle = $scope.getCharacteristicHandler("669a4ed1-19e5-4b9c-87b2-03dc1aa53b33");
    if(handle == -1) {
      return;
    }
    $rootScope.addLog("Enable notification");
    evothings.ble.enableNotification(service.handle, handle, function(data) {
      $rootScope.addLog("Success get msg enableNotification : "+data + ", "+JSON.stringify(data));
    }, function() {
      $rootScope.addLog("Error get msg enableNotification: "+err);
    })
  }


  $scope.getServiceHandler = function(service) {
      var service = undefined;
      for(var i=0; i < $scope.listServices.length; i++) {
        if($scope.listServices[i].uuid == "669a4ed0-19e5-4b9c-87b2-03dc1aa53b33") {
          service = $scope.listServices[i];
        }
      }

     // $rootScope.addLog("Finished needed services. Searched "+JSON.stringify(service));


      return service;
  }

  $scope.getCharacteristicHandler = function(uuid) {
    for(var i=0; i < $scope.listServices.length; i++) {
      if($scope.listServices[i].uuid == "669a4ed0-19e5-4b9c-87b2-03dc1aa53b33") {
        for(var j=0; j < $scope.listServices[i].characteristics.length; j++) {
          if($scope.listServices[i].characteristics[j].uuid == uuid) {
            return $scope.listServices[i].characteristics[j].handle;
          }
        }
      }
    }

    $rootScope.addLog("Not found characteristics: ");

    return -1;
  }

  $scope.setSSIDByteListener = function() {
    $rootScope.addLog("setSSIDByteListener");
    var service = $scope.getServiceHandler();
    var handle = $scope.getCharacteristicHandler("669a4ed2-19e5-4b9c-87b2-03dc1aa53b33");
    if(handle == -1) {
      return;
    }
    $rootScope.addLog("Start writeCharacteristic: " + evothings.ble.toUtf8($scope.data.ssid));
    evothings.ble.writeCharacteristic(service.handle, handle, evothings.ble.toUtf8($scope.data.ssid), function(data){
        $rootScope.addLog("Success write ssid setSSIDByteListener : "+data + ", "+JSON.stringify(data));
        $scope.writePassword();
    }, function(err) {
          $rootScope.addLog("Error write ssid setSSIDByteListener: "+err);
    });
  }

//  $scope.setSSIDByteNoListener = function() {
//    $rootScope.addLog("setSSIDByteNoListener");
//    var service = $scope.getServiceHandler();
//    var handle = $scope.getCharacteristicHandler("669a4ed2-19e5-4b9c-87b2-03dc1aa53b33");
//    if(handle == -1) {
//      return;
//    }
//    $rootScope.addLog("Start writeCharacteristic: " + evothings.ble.toUtf8($scope.data.ssid));
//    evothings.ble.writeCharacteristicWithoutResponse(service.handle, handle, evothings.ble.toUtf8($scope.data.ssid), function(data){
//        $rootScope.addLog("Success write ssid setSSIDByteNoListener : "+data + ", "+JSON.stringify(data));
//    }, function(err) {
//          $rootScope.addLog("Error write ssid setSSIDByteNoListener: "+err);
//    });
//  }
//
//  $scope.setSSIDStringListener = function() {
//    $rootScope.addLog("setSSIDStringListener");
//    var service = $scope.getServiceHandler();
//    var handle = $scope.getCharacteristicHandler("669a4ed2-19e5-4b9c-87b2-03dc1aa53b33");
//    if(handle == -1) {
//      return;
//    }
//    var datassid = {
//      buffer: $scope.data.ssid
//    }
//    $rootScope.addLog("Start writeCharacteristic: "+JSON.stringify(datassid));
//    evothings.ble.writeCharacteristic(service.handle, handle, datassid, function(data){
//        $rootScope.addLog("Success write ssid setSSIDStringListener : "+data + ", "+JSON.stringify(data));
//    }, function(err) {
//          $rootScope.addLog("Error write ssid setSSIDStringListener: "+err);
//    });
//  }
//
//  $scope.setSSIDStringNoListener = function() {
//    $scope.enableNotification();
//    var datassid = {
//      buffer: $scope.data.ssid
//    }
//    $rootScope.addLog("setSSIDStringNoListener: "+JSON.stringify(datassid));
//    var service = $scope.getServiceHandler();
//    var handle = $scope.getCharacteristicHandler("669a4ed2-19e5-4b9c-87b2-03dc1aa53b33");
//    if(handle == -1) {
//      return;
//    }
//
//    $rootScope.addLog("Start writeCharacteristic:"+JSON.stringify(datassid));
//    evothings.ble.writeCharacteristicWithoutResponse(service.handle, handle, datassid, function(data){
//        $rootScope.addLog("Success write ssid setSSIDStringNoListener : "+data + ", "+JSON.stringify(data));
//    }, function(err) {
//          $rootScope.addLog("Error write ssid setSSIDStringNoListener: "+err);
//    });
//  }

  // $scope.writeSSID = function(ssid) {
  //   $rootScope.addLog("Start write ssid: "+ssid);
  //   var handle = $scope.getCharacteristicHandler("669a4ed2-19e5-4b9c-87b2-03dc1aa53b33");
  //   if(handle == -1) {
  //     $rootScope.addLog("Not found handle of Characteristic");
  //     return;
  //   }
  //   $rootScope.addLog("Start writeCharacteristic");
  //   evothings.ble.writeCharacteristic(service.handle, handle, ssid, function onSuccess(data){
  //       $rootScope.addLog("Success write ssid: "+data + ", "+JSON.stringify(data));
  //        $scope.writePassword($scope.data.password);
  //     }, function(err) {
  //         $rootScope.addLog("Error write ssid: "+err);
  //     });

  //   evothings.ble.enableNotification(service.handle, handle, function onSuccess(data){
  //         $rootScope.addLog("Success Notification ssid: "+data + ", "+JSON.stringify(data));
  //     }, function(err) {
  //         $rootScope.addLog("Error Notification ssid: "+err);
  //     });
  // }

   $scope.writePassword = function(password) {
     $rootScope.addLog("Start write password: "+$scope.data.password);
     var service = $scope.getServiceHandler();
     var handle = $scope.getCharacteristicHandler("669a4ed3-19e5-4b9c-87b2-03dc1aa53b33");
     if(handle == -1) {
       $rootScope.addLog("Not found handle of Characteristic");
       return;
     }
     $rootScope.addLog("Start writeCharacteristic");
     evothings.ble.writeCharacteristic(service.handle, handle, evothings.ble.toUtf8($scope.data.password), function onSuccess(data){
         $rootScope.addLog("Success write password: "+data + ", "+JSON.stringify(data));
         $scope.writeHash($scope.data.ssid, $scope.data.password);
       }, function(err) {
           $rootScope.addLog("Error write password: "+err);
       });
     evothings.ble.enableNotification(service.handle, handle, function onSuccess(data){
           $rootScope.addLog("Success Notification password: "+data + ", "+JSON.stringify(data));
       }, function(err) {
           $rootScope.addLog("Error Notification password: "+err);
       });
   }

   $scope.writeHash = function(ssid, password) {
     $rootScope.addLog("Start write hash: "+ssid+", "+password);

     var rawhash = ssid+password+"secret-key";

     var hash = sha256(rawhash);

     var newHash = hash.substring(0, 20);
     $rootScope.addLog("Calculate hash: "+newHash);
     var service = $scope.getServiceHandler();
     var handle = $scope.getCharacteristicHandler("669a4ed4-19e5-4b9c-87b2-03dc1aa53b33");
     if(handle == -1) {
       $rootScope.addLog("Not found handle of Characteristic");
       return;
     }

     $rootScope.addLog("Start writeCharacteristic");
     evothings.ble.writeCharacteristic(service.handle, handle, evothings.ble.toUtf8(newHash), function onSuccess(data){
         $rootScope.addLog("Success write hash: "+data + ", "+JSON.stringify(data));
       }, function(err) {
           $rootScope.addLog("Error write hash: "+err);
       });
     evothings.ble.enableNotification(service.handle, handle, function onSuccess(data){
           $rootScope.addLog("Success Notification hash: "+data + ", "+JSON.stringify(data));
       }, function(err) {
           $rootScope.addLog("Error Notification hash: "+err);
       });
   }



  var connectResult = $stateParams.connectResult;
  if(connectResult !== null) {



  $rootScope.addLog("Start load all services");
  evothings.ble.readAllServiceData(connectResult.deviceHandle, function(services) {
    $rootScope.addLog("Founded services");
    $rootScope.addLog(JSON.stringify(services));

    $scope.listServices = [];
    for (var i=0; i < services.length; i++)
    {
      var service = services[i];

      $rootScope.addLog('s'+service.handle+': '+service.type+' '+service.uuid+'. '+service.characteristics.length+' chars.');
      $rootScope.addLog(JSON.stringify(service));

      $scope.$apply(function () {
            $scope.listServices.push(service);
            //$scope.enableNotification();
      });


      // for (var characteristicIndex in service.characteristics)
      // {
      //   var characteristic = service.characteristics[characteristicIndex];
      //   console.log(' c'+characteristic.handle+': '+characteristic.uuid+'. '+characteristic.descriptors.length+' desc.');
      //   console.log(formatFlags('  properties', characteristic.properties, ble.property));
      //   console.log(formatFlags('  writeType', characteristic.writeType, ble.writeType));

      //   var $characteristic = $characteristicList.addCollapsible({title: 'c' + characteristic.handle + ': ' +
      //     characteristic.uuid + '. ' + characteristic.descriptors.length + ' desc.'});

      //   var $descriptorList;
      //   if (characteristic.descriptors.length > 0)
      //     $descriptorList = $characteristic.addListView();

      //   for (var descriptorIndex in characteristic.descriptors)
      //   {
      //     var descriptor = characteristic.descriptors[descriptorIndex];
      //     console.log('  d'+descriptor.handle+': '+descriptor.uuid);

      //     var $descriptor = $descriptorList.addListViewItem({text: 'd'+descriptor.handle+': '+descriptor.uuid});

      //     // This be the human-readable name of the characteristic.
      //     // if (descriptor.uuid == "00002901-0000-1000-8000-00805f9b34fb")
      //     // {
      //     //   console.log("rd "+descriptor.handle);
      //     //   // need a function here for the closure, so that variables retain proper values.
      //     //   // without it, all strings would be added to the last descriptor.
      //     //   (function(descriptorHandle, characteristicElement, lvi)
      //     //   {
      //     //     ble.readDescriptor(deviceHandle, descriptorHandle, function(data)
      //     //     {
      //     //       var readableData = ble.fromUtf8(data);
      //     //       console.log("rdw "+descriptorHandle+": "+readableData);
      //     //       characteristicElement.collapsibleTitleElm().prepend(readableData + ' ');
      //     //     },
      //     //     function(errorCode)
      //     //     {
      //     //       console.log("rdf "+descriptorHandle+": "+errorCode);
      //     //       lvi.prepend('rdf ' + errorCode);
      //     //     });
      //     //   })(descriptor.handle, $characteristic, $descriptor);
      //     // }
      //   }
      // }
    }


  }, function(errorCode)
  {
    $rootScope.addLog('readAllServiceData error: ' + errorCode);
  });
}


})

.controller('ChatDetailCtrl', function($scope, $stateParams, Chats) {
  $scope.chat = Chats.get($stateParams.chatId);
})

.controller('AccountCtrl', function($scope) {
  $scope.settings = {
    enableFriends: true
  };
});
